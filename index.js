
const FIRST_NAME = "Radu";
const LAST_NAME = "Tofan";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function initCaching() {
    var container={};
    var cache = {
    pageAccessCounter:function(section){
        if(!section && !container.home)
        {
            container.home=1;
        }
        else if(!section && container.home>=1)
        {
            container.home++;
        }
        else if(!container[section.toLowerCase()])
        {
            container[section.toLowerCase()]=1;
        }
        else {
            container[section.toLowerCase()]++;
        }
    },
    getCache:function(){
       return container;
    }
}

    return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

